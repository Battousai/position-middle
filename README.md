# Position element in the middle

Adds classes and mixins to center element horizontally and vertically.

## Installing with bower:

Run `bower i --save position-middle=git@bitbucket.org:Battousai/position-middle.git#~1.0.0`
in your terminal
